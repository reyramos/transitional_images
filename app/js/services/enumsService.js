/**
 * # Global string enumerations file
 *
 * Any strings that are used as a part of application logic should be defined
 * here, and their object key be referenced instead.
 *
 * This is especially useful in cases like $on event listeners where a typo in
 * the name would not ever error, but would simply never catch.
 *
 * By the extra work of defining enumerations for all strings and using those
 * enumerations avoids this by at least throwing undefined argument errors.
 *
 */

/**
 * NOTE, I'm in the processing of moving the enums.socket.requestTypes into
 * the enums.requestTypes
 */

// @TODO: Move this to enums directory and rename to globalEnums.
angular.module('app').factory
('enumsService'
	, function () {
		// @TODO: Get rid of the services in sockets and requestTypes.
		var enums = {
			site: {
				name: 'Palms & Ponies'
			},
			rest: {
				url:'api'
				, post: 'rest:post'
				, get: 'rest:get'
				, from: 'PHP RESTful:api'
			},
			navTree: [
				{name: 'about', route: 'toggleModal:about',icon:'icon-user', subTree: []}
				,{name: 'registration', route: 'toggleModal:registration',icon:'icon-file', subTree: []}
				,{name: 'class info'
					, route: 'toggleModal:class info'
					, icon: 'icon-file'
					, subTree:[
//						{name:'Saturday Class List', route: '#/class-saturday'}
//						,{name:'Sunday Class List', route: '#/class-sunday'}
//						,{name:'Breed Assignment', route: '#/breed'}
//						,{name:'Division Definitions', route: '#/divisions'}
//						,{name:'Entry Info', route: '#/entry'}
//						,{name:'Guidelines', route: '#/guidelines'}
//						,{name:'Donations', route: '#/donate'}
//						,{name:'Wiki', route: '#/wiki'}
					]}
				,{name: 'contact', route: 'toggleModal:contact',icon:'icon-envelope', subTree: []}
			],
			routes: {
				PRIVACY: 'toggleModal:privacy'
			},
			forms:{
				type:'formSubmission'
				,contact: 'contactForm'
			},
			key:'7a262df42c8dbad9e7c1e67aad9f894e'


		}

		return enums

	}
)
