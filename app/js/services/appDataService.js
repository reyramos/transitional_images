angular.module('app').service
	( 'appDataService'
	, function
		( $rootScope
		, $http
		, $log
		, enumsService
		, $templateCache
		){
		
	var service = {}
	service.POST = function(data, callback){
		$http({
			method : 'POST',
			url : enumsService.rest.url,
			data : data,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'data': JSON.stringify(data)
			}
		}).then(function(response) {
				var result = {
					'status':response.status == 200?'OK':'FAIL'
					,'data':response.data
				}
				callback.call(this, result)
			},function(response){
				callback.call(this, response)
			})
	}


	return service
})