# Snowbird.com Mobile menu example #

## About ##
Demonstrate the use of CSS transform <https://developer.mozilla.org/en-US/docs/Web/CSS/transform>
Illustrate the menu transform from top cube to client view on the screen



## Requirements ##

  * Node.js <http://nodejs.org/>

## Development Environment Setup ##

  1. Install Node.JS
	
	Use your system package manager (brew,port,apt-get,yum etc)

  2. Install global Bower, Grunt and PhantomJS commands, this step is not necessary,
  because once you perform grunt build it will install all the necessary files from package.json

	  ``bash
		sudo npm install -g grunt-cli bower phantomjs
	  ``


## Documentation Generation ##
  
	1. Generate documentation with grunt
	  ``bash
	  grunt dox
	  ``

## Development Work-flow ##

  1. Build Development/Distribution

	  ``bash
	  grunt
	  or
	  grunt build
	  ``

  2. Make changes (reflected live in browser) while in development.
     If you make changes and want to reflect in distribution run step 1


  3. Run unit tests
	 Still trying to learn and practice unit testing during the meantime read Angular JS <http://angularjs.org/>

## Deployment ##

  1. Run build scripts

	  ``bash
	  grunt build
		``

  2. Serve compiled, production-ready files from the 'dist' directory
	  ``bash
	  grunt server:dist
		``













